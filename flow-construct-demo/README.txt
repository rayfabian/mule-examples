Note : The mule project is inside a git repo that has multiple projects
The project has 4 flows and 1 subflow
1. flow-construct-demoFlow1 has an endpoint /async1 that demonstrate how to make a asynchronous flow using async, that async internally executes a subflow.
2. flow-construct-demoFlow2 has an endpoint /async2, it demonstates how to make asychronous flow using an one-way exchange pattern connector like VM.
3. flow-construct-demo-vm-flow is flow that accepts request using VM connector
4. flow-construct-demoFlow3 has an endpoint /synch, it demonstrates a synchronous flow, it also shows how to retrieve messages from a database connector when executing a select statement.
 