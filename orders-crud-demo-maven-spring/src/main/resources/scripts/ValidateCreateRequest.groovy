package scripts
import groovy.json.JsonSlurper
def request = new JsonSlurper().parseText(payload)
if (request.get("noOfItems")!=null && request.get("customerId")!=null && request.get("description")!=null ){
	return payload
}else{
	throw new Exception("INVALID_INPUT")
}