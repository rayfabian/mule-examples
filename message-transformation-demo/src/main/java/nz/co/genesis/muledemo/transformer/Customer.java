package nz.co.genesis.muledemo.transformer;

public class Customer {
	private String accountNo;
	private String name;
	private Address address;

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public String toCSV(){
		return this.accountNo+ "," + 
				this.name + "," + 
				this.address.getCity() + "," +
				this.address.getCountry() ;
	}
}
