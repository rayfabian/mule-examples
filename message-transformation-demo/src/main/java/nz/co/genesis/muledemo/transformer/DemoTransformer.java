package nz.co.genesis.muledemo.transformer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;

public class DemoTransformer extends AbstractTransformer {

	private static Log log = LogFactory.getLog(DemoTransformer.class);

	@SuppressWarnings("unchecked")
	@Override
	protected Object doTransform(Object src, String enc) throws TransformerException {
		log.info("log from java transformer");
		
		JSONParser parser = new JSONParser();		
		JSONObject jsonPayload = null;
		try {
			jsonPayload = (JSONObject) parser.parse((String) src);
			jsonPayload.put("messageId", "111111");
		} catch (ParseException e) {			
			e.printStackTrace();
		}				
		
		return (Object) jsonPayload.toJSONString();
	}

}
