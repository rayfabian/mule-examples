package nz.co.genesis.muledemo.transformer;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class DemoComponent  implements Callable{

	@SuppressWarnings("unchecked")
	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		eventContext.getMessage().setInvocationProperty("testVar", "Test Variable from Java");
		eventContext.getMessage().setOutboundProperty("component", "Java Component");
		Object payload = eventContext.getMessage().getPayload();
		JSONParser parser = new JSONParser();		
		JSONObject jsonPayload = null;
		try {
			jsonPayload = (JSONObject) parser.parse((String) payload);
			jsonPayload.put("messageId", "2222222");
		} catch (ParseException e) {			
			e.printStackTrace();
		}			
		
		return (Object) jsonPayload.toJSONString();
	
	}
	

}
