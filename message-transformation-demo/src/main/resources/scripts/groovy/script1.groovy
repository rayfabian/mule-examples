package scripts.groovy
import groovy.json.*
import org.mule.api.transport.PropertyScope
import groovy.util.logging.Log
log.info('Accessing flow variables 1 :' + flowVars['testVar'])
log.info('Accessing flow variables 2 :' + flowVars.testVar)
log.info('Accessing flow variables using message :' + message.getProperty('testVar', ,PropertyScope.INVOCATION))
log.info('Accessing inbound variables using message' + message.getProperty('http.method', ,PropertyScope.INBOUND))

//update or create new flow var
flowVars.testVar = 'some value from groovy'
flowVars.GroovyVar = 'var crated from groovy'
log.info('showing variable updated in groovy :' + flowVars['testVar'])
log.info('showing varibale created in groovy :' + flowVars.GroovyVar)
//update message property
message.setProperty('content-type','application/json' ,PropertyScope.OUTBOUND)
def contentType = message.getProperty('content-type', ,PropertyScope.INBOUND)
log.info("showing updated message properties : $contentType")



def jsonSlurper= new JsonSlurper()
def objPayload = jsonSlurper.parseText(payload)
objPayload.put('company', 'Genesis')
objPayload.get('customer').get('address').putAt('postcode', 123)
result = JsonOutput.toJson(objPayload)
log.info(result)
return result