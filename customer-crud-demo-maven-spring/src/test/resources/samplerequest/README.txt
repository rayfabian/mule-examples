#Create Customer
POST http://localhost/customers
{	
	"name": "Raymundo",
    "address": "Pampanga Philippines"
}

#Get customer information with ID 1
GET http://localhost/customers/1

#Update Customer
POST http://localhost/customers/1
{	
	"name": "Raymundo Fabian",
    "address": "Pampanga Philippines"
}

#Get all customers
GET http://localhost/customers

#Delete customer 
DELETE http://localhost/customers/1

