package scripts
import groovy.json.JsonOutput
def result = []
payload?.each{
	p->
	result.add([id : "${p.CUSTOMER_ID}" ,name : "${p.NAME}", address : "${p.ADDRESS}"])
}
return JsonOutput.toJson(result)