package scripts
import groovy.json.*
import scripts.FieldValidator
def jsonSlurper= new JsonSlurper()
def jsonPayload = jsonSlurper.parseText(payload)
def fields =["name", "address"]

if (!flowVars.varCustomerId.isNumber()){
	throw new Exception("INVALID_INPUT")
}

FieldValidator.validate(jsonPayload, fields);

return payload