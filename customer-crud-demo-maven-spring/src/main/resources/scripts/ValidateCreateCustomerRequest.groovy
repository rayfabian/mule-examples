package scripts
import groovy.json.*
def jsonSlurper= new JsonSlurper()
def requestPayload = jsonSlurper.parseText(payload)
def fields =["name", "address"]
FieldValidator.validate(requestPayload, fields);

return payload