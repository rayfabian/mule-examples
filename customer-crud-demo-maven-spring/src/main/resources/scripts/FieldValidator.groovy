package scripts
static void validate(def jsonPayload, def fields){
	if (jsonPayload.size() == fields.size()){
		fields.each{ field ->
			if (jsonPayload.get("${field}")==null){
				throw new Exception("INVALID_INPUT")
			}
		}
	}else{
		throw new Exception("INVALID_INPUT")
	}
}