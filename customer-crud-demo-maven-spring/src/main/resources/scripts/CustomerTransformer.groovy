package scripts
import groovy.json.JsonOutput
def result = [id : "${payload[0].CUSTOMER_ID}".toInteger(), name : "${payload[0].NAME}", address : "${payload[0].ADDRESS}"]
return JsonOutput.toJson(result)